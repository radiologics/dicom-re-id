# XNAT IQ - DICOM Re-identification

 Re-identify DICOM tags from XNAT IQ clinical metadata and send to a DICOM SCP receiver. Works as a standalone script, a docker container, and includes `command.json` to run as XNAT Container Service.

## Installation

To run as a script, clone repo and:

```bash
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python main.py --help
```

To run as a docker container, clone repo and:

```bash
docker build . -t dicom-re-id
docker run -it -v /path/to/dicom:/input dicom-re-id python main.py --help
```

Example Docker run where XNAT IQ and a DICOM receiver are running on localhost:

```bash
docker run -it -v /path/to/local/dicom:/input dicom-re-id \
  python main.py --project Pr --subject Su --session Se \
    --host localhost --user USER --pass *** \
    --peer host.docker.internal --port 4242 --aetitle ORTHANC \
    --dicomdir /input --tags 0010,0010:0010,0020
```

## Usage

```
$ python main.py --help
usage: main.py [-h] --host HOST --username USERNAME --password PASSWORD --peer PEER --port PORT --aetitle
               AETITLE --project PROJECT --subject SUBJECT --session SESSION --dicomdir DICOMDIR --tags TAGS

Add clinical identifiers back to DICOM and send to SCP receiver. The XNAT host must have the IQ metadata
plugin installed. XNAT Project,Subject,Session is used to route the session back to the desired place.

options:
  -h, --help           show this help message and exit
  --host HOST          XNAT IQ Hostname
  --username USERNAME  XNAT IQ Username
  --password PASSWORD  XNAT IQ Password
  --peer PEER          DICOM SCP Destination
  --port PORT          DICOM SCP Port
  --aetitle AETITLE    Destination AE Title
  --project PROJECT    XNAT Project ID
  --subject SUBJECT    XNAT Subject Label
  --session SESSION    XNAT Session Label
  --dicomdir DICOMDIR  Input directory for DICOM files
  --tags TAGS          List of DICOM tags to re-identify, colon separated, e.g., 0010,0010:0010,0020
```