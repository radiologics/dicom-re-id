import os
import sys
import json
import shutil
import argparse

import requests
from pydicom import dcmread, datadict
from pynetdicom import AE, AllStoragePresentationContexts, StoragePresentationContexts, debug_logger
# debug_logger()
# print("TEST STUDYINSTANCEUID SET")
# test_stuid = ''

parser = argparse.ArgumentParser(
    description="Add clinical identifiers back to DICOM and send to SCP receiver.\n" + 
                "The XNAT host must have the IQ metadata plugin installed.\n" +
                "XNAT Project,Subject,Session is used to route the session back to the desired place.")
parser.add_argument("--host", help="XNAT IQ Hostname", required=True)
parser.add_argument("--username", help="XNAT IQ Username", required=True)
parser.add_argument("--password", help="XNAT IQ Password", required=True)
parser.add_argument("--peer", help="DICOM SCP Destination", required=True)
parser.add_argument("--port", help="DICOM SCP Port", default="8104", required=True)
parser.add_argument("--aetitle", help="Destination AE Title", required=True)
parser.add_argument("--project", help="XNAT Project ID", required=True)
parser.add_argument("--subject", help="XNAT Subject Label", required=True)
parser.add_argument("--session", help="XNAT Session Label", required=True)
parser.add_argument("--dicomdir", help="Input directory for DICOM files", default="/input", required=True)
parser.add_argument("--tags", help="List of DICOM tags to re-identify, colon separated," +
                    " e.g., 0010,0010:0010,0020", required=True)
args = parser.parse_args()

class DicomStudy:
    datasets = []
    reid_tags = []
    clin_meta = None

dicom_study = DicomStudy()

def main():
    parse_params()
    # Load DICOM study into memory
    load_dicom_datasets()
    # Query IQ API by StudyInstanceUID
    get_iq_meta()
    # Replace DICOM tags with IQ API results
    re_identify()
    # Send to DICOM receiver
    dcm_send()

def parse_params():
    """ Check various command line params for accuracy
    """
    print("-- Checking command line arguments")

    _validate_dicomdir()
    _validate_tags()
    _session_exists() # TODO

def _validate_dicomdir():
    if not os.path.isdir(args.dicomdir):
        print("{} is not a directory. Exiting...".format(args.dicomdir))
        raise SystemExit(1)

    if not os.listdir(args.dicomdir):
        print("{} is empty. Exiting...".format(args.dicomdir))
        raise SystemExit(1)

    print('DICOM directory exists and contains files')

def _validate_tags():
    for t in args.tags.split(':'):
        # Each tag should contain a comma and have length of 9
        if not ',' in t or len(t) != 9:
            print('DICOM tags parameter appears to be malformed.')
            print('Requres a colon separated list of group,element combinations, XXXX,XXXX')
            print('For example to re-id PatientName and PatientID, 0010,0010:0010,0020')
            raise SystemExit(1)

        dicom_study.reid_tags.append(t)

    print('DICOM tags parameter appears properly formatted.')
    print('found {} tags'.format(len(dicom_study.reid_tags)))

def _session_exists():
    # TODO Probably shouldn't assume the command line args without any checks.
    # From Container Service, it will be inferred from XNAT context,
    #   but from command line, anything could be entered. Need to check existence.
    pass

def load_dicom_datasets():
    """ Load DICOM study into memory from dicomdir param
    """
    print("\n-- Loading DICOM directory into memory")

    for root, dirs, files in os.walk(args.dicomdir):
        for f in files:
            if '.dcm' not in f:
                continue
            fin = os.path.join(root, f)
            try:
                ds = dcmread(fin)
                dicom_study.datasets.append(ds)
            except Exception as e:
                print('Error reading file {}'.format(fin))

    if not dicom_study.datasets:
        print("No DICOM files found in dicomdir {}".format(args.dicomdir))
        raise SystemExit(1)

    print("found {} DICOM file(s)".format(len(dicom_study.datasets)))

def get_iq_meta():
    """ Query IQ API by StudyInstanceUID
    """
    print("\n-- Getting DICOM metadata from IQ API")

    # stuid = test_stuid
    stuid = dicom_study.datasets[0].StudyInstanceUID
    uri = '/xapi/iq/findbyuid?suid={}'.format(stuid)
    url = args.host + uri

    try:
        r = requests.get(url, auth=(args.username, args.password))
        r.raise_for_status()
    except requests.exceptions.HTTPError as err:
        if (r.status_code == 404):
            print("Make sure that study instance {} exists on {}".format(stuid, args.host))
            print("Also make sure it has been indexed and exists in IQ metadata")
        raise SystemExit(err)
    except requests.exceptions.Timeout:
        printErr("Request timeout")
    except requests.exceptions.RequestException as e:
        raise SystemExit(e)

    print('Requested URL: {}'.format(url))
    print("HTTP status: {}".format(r.status_code))

    try:
        json_str = r.json()['dicomMetadataSet'][0]['data']
    except Exception as e:
        print(e)
        printErr(e)
        printErr("No JSON found in the response")
        raise SystemExit(e)

    try:
        dicom_study.clin_meta = json.loads(json_str)
    except Exception as e:
        print(e)
        print("Failed to parse JSON\n{}".format(json_str))
        raise SystemExit(e)

    print('loaded IQ meta json')

def re_identify():
    """
    https://wiki.xnat.org/documentation/how-to-use-xnat/image-session-upload-methods-in-xnat/how-xnat-scans-dicom-to-map-to-project-subject-session
    Write XNAT Project, Subject, Session to PatientComments field, 
      which should cause a Conflict or Merge in XNAT prearchive
    Replace PatientID, AccessionNumber, and PatientName with API results
    """
    print("\n-- Re-identifying DICOM")

    patient_comments_tag = [0x0010,0x4000]
    comment_str = "Project:{proj} Subject:{subj} Session:{sess}".format(
        proj=args.project,
        subj=args.subject,
        sess=args.session
    )
    vr = datadict.dictionary_VR(patient_comments_tag)
    success = 0

    for ds in dicom_study.datasets:
        ds.add_new(patient_comments_tag, vr, comment_str)

        for tag in dicom_study.reid_tags:
            tag_str = '({})'.format(tag)
            tag_hex = _str_to_hex(tag)

            try:
                iq_val = "TEST"
                # iq_val = dicom_study.clin_meta[tag_str]['Value'][0]
            except Exception as e:
                print('IQ field does not exist for tag {}'.format(tag_str))
                raise SystemExit(e)
                
            try:
                ds[tag_hex].value = iq_val
                success += 1
            except Exception as e:
                printErr('Error setting tag {} in dataset. Trying to add tag.'.format(tag))

                try:
                    vr = datadict.dictionary_VR(tag_hex)
                    ds.add_new(tag_hex, vr, iq_val)
                    printErr("Added successfully")
                    success += 1
                except Exception as e:
                    print('Error adding new tag {}'.format(tag))
                    raise SystemExit(e)

    # Number of successes divided by the number of tags per study
    success = int(success / len(dicom_study.reid_tags))
    print('{} of {} files re-identified successfully'.format(
        success, len(dicom_study.datasets)))
    print('re-identification complete')

def dcm_send():
    """ Use pynetdicom to send to SCP receiver
    """
    print('\n-- Sending DICOM to SCP at {} port {}'.format(args.peer, args.port))

    # TODO failing for some SOPClassUIDs
    # https://github.com/pydicom/pynetdicom/issues/459
    # ae = AE()
    # ae.add_requested_context('1.3.12.2.1107.5.9.1')
    # Does not work due to a max of 128 Storage Contexts

    ae = AE()
    ae.requested_contexts = StoragePresentationContexts
    assoc = ae.associate(args.peer, int(args.port), ae_title=args.aetitle)
    success = 0

    if assoc.is_established:
        for ds in dicom_study.datasets:
            # print(ds.SOPClassUID)
            try:
                status = assoc.send_c_store(ds)
                success += 1
            except Exception as e:
                print('C-STORE failed. Check stderr for more info.')
                printErr(e)
                # raise SystemError(e)

            if not status:
                printErr('Connection timed out, was aborted or received invalid response')
        assoc.release()
    else:
        printErr('Association rejected, aborted or never connected')

    if not success:
        raise SystemExit("all DICOM failed to send")

    print('{} of {} files sent successfully'.format(
        success, len(dicom_study.datasets)))
    print('c-store complete')

def printErr(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def _str_to_hex(tag):
    try:
        group_num = hex(int(tag.split(',')[0], 16))
        elem_num = hex(int(tag.split(',')[1], 16))
    except Exception as e:
        print('Error converting tag to hex: {}'.format(tag))
        raise SystemExit(e)

    return group_num, elem_num

if __name__ == '__main__':
    main()